package app.test.com.twitter_lib.api.auth;


import android.support.annotation.NonNull;

import app.test.com.twitter_lib.api.ApiDispatcher;
import app.test.com.twitter_lib.api.ApiException;
import app.test.com.twitter_lib.repository.AuthRepository;
import app.test.com.twitter_lib.util.Util;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Authenticator implements Callback<AuthResponse> {

    private AuthApi authApi;
    private ApiDispatcher apiDispatcher;
    private AuthRepository authRepository;

    public Authenticator(AuthApi authApi, ApiDispatcher apiDispatcher, AuthRepository authRepository) {
        this.authApi = authApi;
        this.apiDispatcher = apiDispatcher;
        this.authRepository = authRepository;
    }

    public void authenticateApp(String key, String secret) {
        Call<AuthResponse> call = authApi.authoriseApp(Util.convertKeyAndSecret(key, secret), "client_credentials");
        call.enqueue(this);
    }


    @Override
    public void onResponse(@NonNull Call<AuthResponse> call, @NonNull Response<AuthResponse> response) {
        if (response.isSuccessful() && response.body().getAcccessToken() != null) {
            authRepository.saveAuthToken(response.body().getAcccessToken());
            apiDispatcher.dispatchOnAuthSuccess(response.body());
        } else {
            apiDispatcher.dispatchOnAuthError(new ApiException(response.code(), response.message()));
        }
    }

    @Override
    public void onFailure(@NonNull Call<AuthResponse> call, @NonNull Throwable t) {
        apiDispatcher.dispatchOnAuthError(new ApiException("Unable to communicate with service", t));
    }

}
