package app.test.com.twitter_lib.api.auth;


import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface AuthApi {

    @FormUrlEncoded
    @POST("oauth2/token")
    Call<AuthResponse> authoriseApp(@Header("Authorization") String authToken, @Field("grant_type") String grantType);
}
