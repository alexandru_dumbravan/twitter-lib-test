package app.test.com.twitter_lib.api;

public enum RequestType {
    AUTH, TWEETS
}
