package app.test.com.twitter_lib.repository;

import android.content.Context;

public class AuthPreferenceStorage extends DefaultPreferenceStorage implements AuthRepository {


    static final String KEY_AUTH_TOKEN = "auth_token";

    public AuthPreferenceStorage(Context context) {
        super(context, "twitter-auth-prefs");
    }


    public void saveAuthToken(String authToken) {
        putString(KEY_AUTH_TOKEN, authToken);
    }

    public String getAuthToken() {
        return getString(KEY_AUTH_TOKEN, "");
    }
}
