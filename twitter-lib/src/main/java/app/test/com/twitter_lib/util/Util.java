package app.test.com.twitter_lib.util;


import android.util.Base64;

public class Util {


    public static String convertKeyAndSecret(String key, String secret) {
        return "Basic " + Base64.encodeToString((key + ":" + secret).getBytes(), Base64.NO_WRAP);
    }

    public static String convertToken(String token) {
        return "Bearer " + token;
    }
}
