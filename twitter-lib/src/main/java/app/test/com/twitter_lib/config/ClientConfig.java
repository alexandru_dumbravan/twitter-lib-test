package app.test.com.twitter_lib.config;


public interface ClientConfig {

    AuthConfig getAuthConfig();

    //Add additional configurations for other apis
}
