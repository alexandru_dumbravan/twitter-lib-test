package app.test.com.twitter_lib.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import app.test.com.twitter_lib.api.auth.AuthResponse;
import app.test.com.twitter_lib.api.tweet.TweetResponse;

public class ApiDispatcher {

    private HashMap<RequestType, List<ApiListener<?>>> listenerMap = new HashMap<>();

    public ApiDispatcher() {
        for (RequestType type : RequestType.values()) {
            listenerMap.put(type, new ArrayList<ApiListener<?>>());
        }
    }

    public void addListener(RequestType requestType, ApiListener<?> listener) {
        List<ApiListener<?>> listeners = listenerMap.get(requestType);
        if (!listeners.contains(listener)) {
            listeners.add(listener);
        }
    }

    public void removeListener(ApiListener<?> listener) {
        for (RequestType requestType : listenerMap.keySet()) {
            listenerMap.get(requestType).remove(listener);
        }
    }

    public void dispatchOnAuthSuccess(AuthResponse authResponse) {
        dispatchOnSuccess(listenerMap.get(RequestType.AUTH), authResponse);
    }

    public void dispatchOnAuthError(ApiException apiException) {
        dispatchOnError(listenerMap.get(RequestType.AUTH), apiException);
    }

    public void dispatchOnTweetSuccess(TweetResponse tweetResponse) {
        dispatchOnSuccess(listenerMap.get(RequestType.TWEETS), tweetResponse);
    }

    public void dispacthOnTweetError(ApiException apiException) {
        dispatchOnError(listenerMap.get(RequestType.TWEETS), apiException);
    }


    private static void dispatchOnSuccess(List<ApiListener<?>> listeners, Object data) {
        for (ApiListener l : listeners) {
            l.onSuccess(data);
        }
    }

    private static void dispatchOnError(List<ApiListener<?>> listeners, ApiException apiException) {
        for (ApiListener l : listeners) {
            l.onError(apiException);
        }
    }

    public HashMap<RequestType, List<ApiListener<?>>> getListenerMap() {
        return listenerMap;
    }
}
