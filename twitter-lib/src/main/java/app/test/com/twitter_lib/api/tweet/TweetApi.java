package app.test.com.twitter_lib.api.tweet;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface TweetApi {

    @GET("1.1/search/tweets.json")
    Call<TweetResponse> searchTweets(@Header("Authorization") String authorization, @Query("q") String searchTerm, @Query("count") int count);

    @GET("1.1/search/tweets.json")
    Call<TweetResponse> searchTweets(@Header("Authorization") String authorization, @Query("q") String searchTerm, @Query("count") int count, @Query("max_id") long maxId, @Query("since_id") long sinceId);
}
