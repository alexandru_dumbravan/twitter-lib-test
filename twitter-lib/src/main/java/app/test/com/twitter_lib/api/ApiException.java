package app.test.com.twitter_lib.api;

/**
 * Created by alexa on 7/9/2017.
 */

public class ApiException extends Exception {

    private int code;

    public ApiException(int code, String message) {
        this(code, message, null);
    }

    public ApiException(String message, Throwable cause) {
        this(-1, message, cause);
    }

    public ApiException(int code, String message, Throwable cause) {
        super(message, cause);
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
