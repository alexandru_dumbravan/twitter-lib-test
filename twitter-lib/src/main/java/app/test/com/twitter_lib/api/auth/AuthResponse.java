package app.test.com.twitter_lib.api.auth;


import com.google.gson.annotations.SerializedName;

public class AuthResponse {

    @SerializedName("token_type")
    private String tokenType;
    @SerializedName("access_token")
    private String acccessToken;

    public String getTokenType() {
        return tokenType;
    }

    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }

    public String getAcccessToken() {
        return acccessToken;
    }

    public void setAcccessToken(String acccessToken) {
        this.acccessToken = acccessToken;
    }
}
