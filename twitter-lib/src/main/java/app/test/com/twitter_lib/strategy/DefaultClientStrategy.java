package app.test.com.twitter_lib.strategy;

import app.test.com.twitter_lib.api.ApiDispatcher;
import app.test.com.twitter_lib.api.ApiListener;
import app.test.com.twitter_lib.api.ApiManager;
import app.test.com.twitter_lib.api.RequestType;
import app.test.com.twitter_lib.api.auth.AuthApi;
import app.test.com.twitter_lib.api.auth.AuthResponse;
import app.test.com.twitter_lib.api.auth.Authenticator;
import app.test.com.twitter_lib.api.tweet.TweetApi;
import app.test.com.twitter_lib.api.tweet.TweetResponse;
import app.test.com.twitter_lib.api.tweet.TweetSearcher;
import app.test.com.twitter_lib.config.ClientConfig;
import app.test.com.twitter_lib.repository.AuthRepository;

public class DefaultClientStrategy {


    Authenticator authenticator;
    TweetSearcher tweetSearcher;
    ApiDispatcher dispatcher;
    ClientConfig clientConfig;
    AuthRepository authRepository;


    public void authoriseApp(ApiListener<AuthResponse> authApiListener) {
        dispatcher.addListener(RequestType.AUTH, authApiListener);
        authenticator.authenticateApp(clientConfig.getAuthConfig().getClientKey(), clientConfig.getAuthConfig().getClientSecret());
    }

    public void searchTweets(String tweetText, int count, ApiListener<TweetResponse> tweetApiListener) {
        dispatcher.addListener(RequestType.TWEETS, tweetApiListener);
        tweetSearcher.searchTweet(authRepository.getAuthToken(), tweetText, count);
    }

    public void searchTweets(String tweetText, int count, long maxId, long sinceId, ApiListener<TweetResponse> tweetApiListener) {
        dispatcher.addListener(RequestType.TWEETS, tweetApiListener);
        tweetSearcher.searchTweet(authRepository.getAuthToken(), tweetText, count, maxId, sinceId);
    }


    public void setAuthenticator(Authenticator authenticator) {
        this.authenticator = authenticator;
    }

    public void setTweetSearcher(TweetSearcher tweetSearcher) {
        this.tweetSearcher = tweetSearcher;
    }

    public void setClientConfig(ClientConfig clientConfig) {
        this.clientConfig = clientConfig;
    }

    public void setDispatcher(ApiDispatcher dispatcher) {
        this.dispatcher = dispatcher;
    }

    public void setAuthRepository(AuthRepository authRepository) {
        this.authRepository = authRepository;
    }
}
