package app.test.com.twitter_lib;

import android.content.Context;

import app.test.com.twitter_lib.api.ApiDispatcher;
import app.test.com.twitter_lib.api.ApiListener;
import app.test.com.twitter_lib.api.ApiManager;
import app.test.com.twitter_lib.api.auth.AuthApi;
import app.test.com.twitter_lib.api.auth.AuthResponse;
import app.test.com.twitter_lib.api.auth.Authenticator;
import app.test.com.twitter_lib.api.tweet.TweetApi;
import app.test.com.twitter_lib.api.tweet.TweetResponse;
import app.test.com.twitter_lib.api.tweet.TweetSearcher;
import app.test.com.twitter_lib.config.ClientConfig;
import app.test.com.twitter_lib.repository.AuthPreferenceStorage;
import app.test.com.twitter_lib.repository.AuthRepository;
import app.test.com.twitter_lib.strategy.AutoLoginClientStrategy;
import app.test.com.twitter_lib.strategy.DefaultClientStrategy;

public class TwitterClient {

    private ClientConfig clientConfig;
    private ApiManager apiManager;
    private ApiDispatcher dispatcher;
    private AuthRepository authRepository;
    private DefaultClientStrategy defaultClientStrategy;


    public TwitterClient(Builder builder) {
        this.clientConfig = builder.clientConfig;
        this.apiManager = builder.apiManager;
        this.dispatcher = builder.dispatcher;
        this.apiManager.init();
        this.authRepository = builder.authRepository;
        if (authRepository == null) {
            if (builder.context == null) {
                throw new IllegalArgumentException("Must specify a context or an auth repository");
            }
            authRepository = new AuthPreferenceStorage(builder.context.getApplicationContext());
        }
        this.defaultClientStrategy = builder.defaultClientStrategy;

        this.defaultClientStrategy.setAuthenticator(new Authenticator(apiManager.createService(AuthApi.class), dispatcher, authRepository));
        this.defaultClientStrategy.setTweetSearcher(new TweetSearcher(apiManager.createService(TweetApi.class), dispatcher));
        this.defaultClientStrategy.setAuthRepository(authRepository);
        this.defaultClientStrategy.setDispatcher(dispatcher);
        this.defaultClientStrategy.setClientConfig(clientConfig);
    }


    public void authoriseApp(ApiListener<AuthResponse> apiListener) {
        defaultClientStrategy.authoriseApp(apiListener);
    }

    public void searchTweets(String tweetText, int count, ApiListener<TweetResponse> apiListener) {
        defaultClientStrategy.searchTweets(tweetText, count, apiListener);
    }

    public void searchTweets(String tweetText, int count, long maxId, long sinceId, ApiListener<TweetResponse> apiListener) {
        defaultClientStrategy.searchTweets(tweetText, count, maxId, sinceId, apiListener);
    }


    public void removeListener(ApiListener<?> apiListener) {
        dispatcher.removeListener(apiListener);
    }


    public static class Builder {

        private ClientConfig clientConfig;
        private ApiManager apiManager = new ApiManager();
        private ApiDispatcher dispatcher = new ApiDispatcher();
        private AuthRepository authRepository;
        private Context context;
        private DefaultClientStrategy defaultClientStrategy = new AutoLoginClientStrategy();

        public Builder() {

        }

        public Builder withClientConfig(ClientConfig clientConfig) {
            this.clientConfig = clientConfig;
            return this;
        }

        public Builder withApiManager(ApiManager apiManager) {
            this.apiManager = apiManager;
            return this;
        }

        public Builder withDispatcher(ApiDispatcher dispatcher) {
            this.dispatcher = dispatcher;
            return this;
        }

        public Builder withAuthRepository(AuthRepository authRepository) {
            this.authRepository = authRepository;
            return this;
        }

        public Builder withContext(Context context) {
            this.context = context;
            return this;
        }

        public Builder withClientStrategy(DefaultClientStrategy defaultClientStrategy) {
            this.defaultClientStrategy = defaultClientStrategy;
            return this;
        }
    }
}

