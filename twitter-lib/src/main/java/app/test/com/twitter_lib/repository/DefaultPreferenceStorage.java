package app.test.com.twitter_lib.repository;


import android.content.Context;
import android.content.SharedPreferences;

class DefaultPreferenceStorage {

    private SharedPreferences sharedPreferences;

    DefaultPreferenceStorage(Context context, String fileName) {
        sharedPreferences = context.getSharedPreferences(fileName, Context.MODE_PRIVATE);
    }

    String getString(String key, String defValue) {
        return sharedPreferences.getString(key, defValue);
    }

    void putString(String key, String value) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.apply();
    }
}
