package app.test.com.twitter_lib.repository;


public interface AuthRepository {

    String getAuthToken();

    void saveAuthToken(String authToken);
}
