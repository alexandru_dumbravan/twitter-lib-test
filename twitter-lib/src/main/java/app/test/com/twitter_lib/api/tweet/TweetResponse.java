package app.test.com.twitter_lib.api.tweet;


import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TweetResponse {

    @SerializedName("statuses")
    private List<Tweet> tweets;

    public List<Tweet> getTweets() {
        return tweets;
    }

    public void setTweets(List<Tweet> tweets) {
        this.tweets = tweets;
    }
}
