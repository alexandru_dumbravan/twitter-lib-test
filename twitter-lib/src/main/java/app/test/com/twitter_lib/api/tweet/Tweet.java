package app.test.com.twitter_lib.api.tweet;

import com.google.gson.annotations.SerializedName;

public class Tweet {

    private long id;
    private String text;
    @SerializedName("created_at")
    private String createdAt;
    private User user;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
