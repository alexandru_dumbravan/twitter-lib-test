package app.test.com.twitter_lib.config;


public interface AuthConfig {

    String getClientKey();

    String getClientSecret();

}
