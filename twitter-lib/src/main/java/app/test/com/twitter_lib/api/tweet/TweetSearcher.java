package app.test.com.twitter_lib.api.tweet;


import app.test.com.twitter_lib.api.ApiDispatcher;
import app.test.com.twitter_lib.api.ApiException;
import app.test.com.twitter_lib.util.Util;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TweetSearcher implements Callback<TweetResponse> {

    private TweetApi tweetApi;
    private ApiDispatcher apiDispatcher;

    public TweetSearcher(TweetApi tweetApi, ApiDispatcher apiDispatcher) {
        this.tweetApi = tweetApi;
        this.apiDispatcher = apiDispatcher;
    }


    public void searchTweet(String bearerToken, String tweetText, int count) {
        Call<TweetResponse> call = tweetApi.searchTweets(Util.convertToken(bearerToken), tweetText, count);
        call.enqueue(this);
    }

    public void searchTweet(String bearerToken, String tweetText, int count, long maxId, long sinceId) {
        Call<TweetResponse> call = tweetApi.searchTweets(Util.convertToken(bearerToken), tweetText, count, maxId, sinceId);
        call.enqueue(this);
    }


    @Override
    public void onResponse(Call<TweetResponse> call, Response<TweetResponse> response) {
        if (response.isSuccessful()) {
            apiDispatcher.dispatchOnTweetSuccess(response.body());
        } else {
            apiDispatcher.dispacthOnTweetError(new ApiException(response.code(), "Unable to find tweets"));
        }
    }

    @Override
    public void onFailure(Call<TweetResponse> call, Throwable t) {
        apiDispatcher.dispacthOnTweetError(new ApiException("Could not communicate to get the tweets", t));
    }
}
