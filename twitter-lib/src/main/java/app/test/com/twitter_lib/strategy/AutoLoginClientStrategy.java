package app.test.com.twitter_lib.strategy;


import app.test.com.twitter_lib.api.ApiException;
import app.test.com.twitter_lib.api.ApiListener;
import app.test.com.twitter_lib.api.auth.AuthResponse;
import app.test.com.twitter_lib.api.tweet.TweetResponse;

public class AutoLoginClientStrategy extends DefaultClientStrategy {


    @Override
    public void searchTweets(final String tweetText, final int count, final long maxId, final long sinceId, final ApiListener<TweetResponse> tweetApiListener) {
        if (isAuthorised()) {
            super.searchTweets(tweetText, count, maxId, sinceId, tweetApiListener);
        } else {
            authoriseApp(new AuthApiListener(tweetApiListener) {

                @Override
                public void onSuccess(AuthResponse data) {
                    super.onSuccess(data);
                    searchTweets(tweetText, count, maxId, sinceId, tweetApiListener);
                }
            });
        }
    }

    @Override
    public void searchTweets(final String tweetText, final int count, final ApiListener<TweetResponse> tweetApiListener) {
        if (isAuthorised()) {
            super.searchTweets(tweetText, count, tweetApiListener);
        } else {
            authoriseApp(new AuthApiListener(tweetApiListener) {

                @Override
                public void onSuccess(AuthResponse data) {
                    super.onSuccess(data);
                    searchTweets(tweetText, count, tweetApiListener);
                }
            });
        }
    }


    private boolean isAuthorised() {
        String token = authRepository.getAuthToken();
        return token != null && !token.isEmpty();
    }


    class AuthApiListener implements ApiListener<AuthResponse> {

        private ApiListener<?> initialListener;

        AuthApiListener(ApiListener<?> initialListener) {
            this.initialListener = initialListener;
        }

        @Override
        public void onSuccess(AuthResponse data) {
            dispatcher.removeListener(this);
        }

        @Override
        public void onError(ApiException apiException) {
            dispatcher.removeListener(this);
            initialListener.onError(new ApiException("Cannot automatically login client", apiException));
        }
    }
}
