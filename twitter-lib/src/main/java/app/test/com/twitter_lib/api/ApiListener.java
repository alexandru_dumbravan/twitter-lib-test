package app.test.com.twitter_lib.api;

public interface ApiListener<T> {

    void onSuccess(T data);

    void onError(ApiException apiException);
}
