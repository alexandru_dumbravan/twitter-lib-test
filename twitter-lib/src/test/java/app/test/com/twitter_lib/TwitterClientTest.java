package app.test.com.twitter_lib;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import app.test.com.twitter_lib.api.ApiDispatcher;
import app.test.com.twitter_lib.api.ApiListener;
import app.test.com.twitter_lib.api.auth.AuthResponse;
import app.test.com.twitter_lib.api.tweet.TweetResponse;
import app.test.com.twitter_lib.repository.AuthRepository;
import app.test.com.twitter_lib.strategy.DefaultClientStrategy;


@RunWith(MockitoJUnitRunner.class)
public class TwitterClientTest {


    private TwitterClient subject;
    @Mock
    private DefaultClientStrategy defaultClientStrategy;
    @Mock
    private AuthRepository authRepository;
    @Mock
    private ApiDispatcher apiDispatcher;

    @Before
    public void setUp() {
        subject = new TwitterClient(new TwitterClient.Builder().withClientStrategy(defaultClientStrategy).withAuthRepository(authRepository).withDispatcher(apiDispatcher));
    }

    @Test
    public void authoriseApp() throws Exception {
        ApiListener<AuthResponse> apiListener = Mockito.mock(ApiListener.class);
        subject.authoriseApp(apiListener);
        Mockito.verify(defaultClientStrategy).authoriseApp(apiListener);
    }

    @Test
    public void searchTweets_default() throws Exception {
        ApiListener<TweetResponse> apiListener = Mockito.mock(ApiListener.class);
        String tweetText = "tweetText";
        int count = 10;
        subject.searchTweets(tweetText, count, apiListener);
        Mockito.verify(defaultClientStrategy).searchTweets(tweetText, count, apiListener);
    }

    @Test
    public void searchTweets_paginated() throws Exception {
        ApiListener<TweetResponse> apiListener = Mockito.mock(ApiListener.class);
        String tweetText = "tweetText";
        int count = 10;
        long sinceId = 1;
        long maxId = 20;
        subject.searchTweets(tweetText, count, maxId, sinceId, apiListener);
        Mockito.verify(defaultClientStrategy).searchTweets(tweetText, count, maxId, sinceId, apiListener);
    }

    @Test
    public void removeListener() throws Exception {
        ApiListener<?> apiListener = Mockito.mock(ApiListener.class);
        subject.removeListener(apiListener);
        Mockito.verify(apiDispatcher).removeListener(apiListener);
    }

}