package app.test.com.twitter_lib.repository;

import android.content.Context;
import android.content.SharedPreferences;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class AuthPreferenceStorageTest {

    private AuthPreferenceStorage subject;
    @Mock
    private SharedPreferences sharedPreferences;


    @Before
    public void setUp() throws Exception {
        Context context = Mockito.mock(Context.class);
        Mockito.when(context.getSharedPreferences(Mockito.anyString(), Mockito.anyInt())).thenReturn(sharedPreferences);
        subject = Mockito.spy(new AuthPreferenceStorage(context));
        Mockito.verify(context).getSharedPreferences("twitter-auth-prefs", Context.MODE_PRIVATE);
        Mockito.when(sharedPreferences.edit()).thenReturn(Mockito.mock(SharedPreferences.Editor.class));
    }

    @Test
    public void saveAuthToken() throws Exception {
        String token = "token";
        subject.saveAuthToken(token);
        Mockito.verify(subject).putString(AuthPreferenceStorage.KEY_AUTH_TOKEN, token);
    }

    @Test
    public void getAuthToken() throws Exception {
        subject.getAuthToken();
        Mockito.verify(subject).getString(AuthPreferenceStorage.KEY_AUTH_TOKEN, "");
    }

}