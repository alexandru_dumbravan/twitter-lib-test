package app.test.com.twitter_lib.api.tweet;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import app.test.com.twitter_lib.api.ApiDispatcher;
import app.test.com.twitter_lib.api.ApiException;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

@RunWith(MockitoJUnitRunner.class)
public class TweetSearcherTest {

    private TweetSearcher subject;
    @Mock
    private TweetApi tweetApi;
    @Mock
    private ApiDispatcher apiDispatcher;


    @Before
    public void setUp() throws Exception {
        subject = new TweetSearcher(tweetApi, apiDispatcher);
    }

    @Test
    public void searchTweet_firstPage() throws Exception {
        String token = "token";
        String text = "text";
        int count = 12;
        Call<TweetResponse> call = Mockito.mock(Call.class);
        Mockito.when(tweetApi.searchTweets(Mockito.anyString(), Mockito.anyString(), Mockito.anyInt())).thenReturn(call);
        subject.searchTweet(token, text, count);
        Mockito.verify(tweetApi).searchTweets("Bearer " + token, text, count);
        Mockito.verify(call).enqueue(subject);
    }

    @Test
    public void searchTweet_paginated() throws Exception {
        String token = "token";
        String text = "text";
        int count = 12;
        long sinceId = 10;
        long maxId = 20;
        Call<TweetResponse> call = Mockito.mock(Call.class);
        Mockito.when(tweetApi.searchTweets(Mockito.anyString(), Mockito.anyString(), Mockito.anyInt(), Mockito.anyLong(), Mockito.anyLong())).thenReturn(call);
        subject.searchTweet(token, text, count, maxId, sinceId);
        Mockito.verify(tweetApi).searchTweets("Bearer " + token, text, count, maxId, sinceId);
        Mockito.verify(call).enqueue(subject);
    }

    @Test
    public void onResponse_success() throws Exception {
        TweetResponse tweetResponse = new TweetResponse();
        Response<TweetResponse> response = Response.success(tweetResponse);
        subject.onResponse(Mockito.mock(Call.class), response);
        Mockito.verify(apiDispatcher).dispatchOnTweetSuccess(tweetResponse);
    }


    @Test
    public void onResponse_error() throws Exception {
        Response<TweetResponse> response = Response.error(401, Mockito.mock(ResponseBody.class));
        ArgumentCaptor<ApiException> argumentCaptor = ArgumentCaptor.forClass(ApiException.class);
        subject.onResponse(Mockito.mock(Call.class), response);
        Mockito.verify(apiDispatcher).dispacthOnTweetError(argumentCaptor.capture());
        Assert.assertEquals(401, argumentCaptor.getValue().getCode());
    }

    @Test
    public void onFailure() throws Exception {
        RuntimeException testException = new RuntimeException("Test exception");
        ArgumentCaptor<ApiException> argumentCaptor = ArgumentCaptor.forClass(ApiException.class);
        subject.onFailure(Mockito.mock(Call.class), testException);
        Mockito.verify(apiDispatcher).dispacthOnTweetError(argumentCaptor.capture());
        Assert.assertEquals("Could not communicate to get the tweets", argumentCaptor.getValue().getMessage());
        Assert.assertEquals(testException, argumentCaptor.getValue().getCause());
    }

}