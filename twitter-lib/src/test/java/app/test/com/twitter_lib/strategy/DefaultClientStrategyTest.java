package app.test.com.twitter_lib.strategy;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import app.test.com.twitter_lib.api.ApiDispatcher;
import app.test.com.twitter_lib.api.ApiListener;
import app.test.com.twitter_lib.api.RequestType;
import app.test.com.twitter_lib.api.auth.AuthResponse;
import app.test.com.twitter_lib.api.auth.Authenticator;
import app.test.com.twitter_lib.api.tweet.TweetResponse;
import app.test.com.twitter_lib.api.tweet.TweetSearcher;
import app.test.com.twitter_lib.config.AuthConfig;
import app.test.com.twitter_lib.config.ClientConfig;
import app.test.com.twitter_lib.repository.AuthRepository;

@RunWith(MockitoJUnitRunner.class)
public class DefaultClientStrategyTest {

    protected DefaultClientStrategy subject;
    @Mock
    protected ClientConfig clientConfig;
    @Mock
    protected ApiDispatcher dispatcher;
    @Mock
    protected AuthRepository authRepository;
    @Mock
    protected Authenticator authenticator;
    @Mock
    protected TweetSearcher tweetSearcher;


    @Before
    public void setUp() throws Exception {
        subject = createSubject();
        subject.setClientConfig(clientConfig);
        subject.setAuthenticator(authenticator);
        subject.setTweetSearcher(tweetSearcher);
        subject.setDispatcher(dispatcher);
        subject.setAuthRepository(authRepository);


    }

    protected DefaultClientStrategy createSubject() {
        return new DefaultClientStrategy();
    }

    @Test
    public void authoriseApp() throws Exception {
        String key = "key";
        String secret = "secret";
        AuthConfig authConfig = Mockito.mock(AuthConfig.class);
        Mockito.when(authConfig.getClientKey()).thenReturn(key);
        Mockito.when(authConfig.getClientSecret()).thenReturn(secret);
        Mockito.when(clientConfig.getAuthConfig()).thenReturn(authConfig);
        ApiListener<AuthResponse> apiListener = Mockito.mock(ApiListener.class);

        subject.authoriseApp(apiListener);
        Mockito.verify(dispatcher).addListener(RequestType.AUTH, apiListener);
        Mockito.verify(authenticator).authenticateApp(key, secret);

    }

    @Test
    public void searchTweets_singlePage() throws Exception {
        String bearerToken = "bearerToken";
        String tweetText = "tweetText";
        int count = 10;
        Mockito.when(authRepository.getAuthToken()).thenReturn(bearerToken);
        ApiListener<TweetResponse> apiListener = Mockito.mock(ApiListener.class);
        subject.searchTweets(tweetText, count, apiListener);
        Mockito.verify(dispatcher).addListener(RequestType.TWEETS, apiListener);
        Mockito.verify(tweetSearcher).searchTweet(bearerToken, tweetText, count);
    }

    @Test
    public void searchTweets_paginated() throws Exception {
        String bearerToken = "bearerToken";
        String tweetText = "tweetText";
        int count = 10;
        long sinceId = 1;
        long maxId = 10;
        Mockito.when(authRepository.getAuthToken()).thenReturn(bearerToken);
        ApiListener<TweetResponse> apiListener = Mockito.mock(ApiListener.class);
        subject.searchTweets(tweetText, count, maxId, sinceId, apiListener);
        Mockito.verify(dispatcher).addListener(RequestType.TWEETS, apiListener);
        Mockito.verify(tweetSearcher).searchTweet(bearerToken, tweetText, count, maxId, sinceId);
    }

}