package app.test.com.twitter_lib.repository;

import android.content.Context;
import android.content.SharedPreferences;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;


@RunWith(MockitoJUnitRunner.class)
public class DefaultPreferenceStorageTest {

    private DefaultPreferenceStorage subject;
    @Mock
    private SharedPreferences sharedPreferences;
    @Mock
    private SharedPreferences.Editor editor;


    @Before
    public void setUp() {
        Context context = Mockito.mock(Context.class);
        Mockito.when(context.getSharedPreferences(Mockito.anyString(), Mockito.anyInt())).thenReturn(sharedPreferences);
        subject = new DefaultPreferenceStorage(context, "test");
        Mockito.verify(context).getSharedPreferences("test", Context.MODE_PRIVATE);

        Mockito.when(sharedPreferences.edit()).thenReturn(editor);
    }

    @Test
    public void getString() throws Exception {
        String key = "key";
        String defValue = "defValue";
        subject.getString(key, defValue);
        Mockito.verify(sharedPreferences).getString(key, defValue);
    }

    @Test
    public void putString() throws Exception {
        String key = "key";
        String value = "value";
        subject.putString(key, value);
        Mockito.verify(sharedPreferences).edit();
        Mockito.verify(editor).putString(key, value);
        Mockito.verify(editor).apply();
    }

}