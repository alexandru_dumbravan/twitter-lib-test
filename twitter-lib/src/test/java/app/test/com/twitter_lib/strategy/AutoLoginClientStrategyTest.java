package app.test.com.twitter_lib.strategy;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import app.test.com.twitter_lib.api.ApiListener;
import app.test.com.twitter_lib.api.auth.AuthResponse;
import app.test.com.twitter_lib.api.tweet.TweetResponse;

@RunWith(MockitoJUnitRunner.class)
public class AutoLoginClientStrategyTest extends DefaultClientStrategyTest {

    @Before
    public void setUp() throws Exception {
        super.setUp();
        Mockito.when(authRepository.getAuthToken()).thenReturn("test");
    }

    @Override
    protected DefaultClientStrategy createSubject() {
        return Mockito.spy(new AutoLoginClientStrategy());
    }

    @Test
    public void searchTweets_singlePageNotLoggedId() throws Exception {
        Mockito.doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                return null;
            }
        }).when(subject).authoriseApp((ApiListener<AuthResponse>) Mockito.any());
        Mockito.when(authRepository.getAuthToken()).thenReturn(null);
        String tweetText = "tweetText";
        int count = 10;
        ArgumentCaptor<ApiListener> argumentCaptor = ArgumentCaptor.forClass(ApiListener.class);
        ApiListener<TweetResponse> apiListener = Mockito.mock(ApiListener.class);
        subject.searchTweets(tweetText, count, apiListener);
        Mockito.verify(subject).authoriseApp(argumentCaptor.capture());
        Assert.assertTrue(AutoLoginClientStrategy.AuthApiListener.class.isInstance(argumentCaptor.getValue()));
    }


    @Test
    public void searchTweets_paginatedNotLoggedId() throws Exception {
        Mockito.doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                return null;
            }
        }).when(subject).authoriseApp((ApiListener<AuthResponse>) Mockito.any());
        Mockito.when(authRepository.getAuthToken()).thenReturn(null);
        String tweetText = "tweetText";
        int count = 10;
        long sinceId = 1;
        long maxId = 10;
        ArgumentCaptor<ApiListener> argumentCaptor = ArgumentCaptor.forClass(ApiListener.class);
        ApiListener<TweetResponse> apiListener = Mockito.mock(ApiListener.class);
        subject.searchTweets(tweetText, count, sinceId, maxId, apiListener);
        Mockito.verify(subject).authoriseApp(argumentCaptor.capture());
        Assert.assertTrue(AutoLoginClientStrategy.AuthApiListener.class.isInstance(argumentCaptor.getValue()));
    }

}