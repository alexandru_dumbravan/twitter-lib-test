package app.test.com.twitter_lib.api.auth;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import app.test.com.twitter_lib.api.ApiDispatcher;
import app.test.com.twitter_lib.api.ApiException;
import app.test.com.twitter_lib.repository.AuthRepository;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;


@RunWith(MockitoJUnitRunner.class)
public class AuthenticatorTest {

    private Authenticator subject;
    @Mock
    private AuthApi authApi;
    @Mock
    private ApiDispatcher apiDispatcher;
    @Mock
    private AuthRepository authRepository;

    @Before
    public void setUp() throws Exception {
        subject = new Authenticator(authApi, apiDispatcher, authRepository);
    }

    @Test
    public void authenticateApp() throws Exception {
        Call<AuthResponse> call = Mockito.mock(Call.class);
        Mockito.when(authApi.authoriseApp(Mockito.anyString(), Mockito.anyString())).thenReturn(call);
        subject.authenticateApp("key", "secret");
        Mockito.verify(authApi).authoriseApp("Basic null", "client_credentials");
        Mockito.verify(call).enqueue(subject);
    }

    @Test
    public void onResponse_success() throws Exception {
        AuthResponse authResponse = new AuthResponse();
        authResponse.setAcccessToken("test");
        Response<AuthResponse> response = Response.success(authResponse);
        subject.onResponse(Mockito.mock(Call.class), response);
        Mockito.verify(authRepository).saveAuthToken(authResponse.getAcccessToken());
        Mockito.verify(apiDispatcher).dispatchOnAuthSuccess(authResponse);
    }

    @Test
    public void onResponse_error() throws Exception {
        Response<AuthResponse> response = Response.error(401, Mockito.mock(ResponseBody.class));
        ArgumentCaptor<ApiException> argumentCaptor = ArgumentCaptor.forClass(ApiException.class);
        subject.onResponse(Mockito.mock(Call.class), response);
        Mockito.verify(apiDispatcher).dispatchOnAuthError(argumentCaptor.capture());
        Assert.assertEquals(401, argumentCaptor.getValue().getCode());
    }

    @Test
    public void onFailure() throws Exception {
        RuntimeException testException = new RuntimeException("Test exception");
        ArgumentCaptor<ApiException> argumentCaptor = ArgumentCaptor.forClass(ApiException.class);
        subject.onFailure(Mockito.mock(Call.class), testException);
        Mockito.verify(apiDispatcher).dispatchOnAuthError(argumentCaptor.capture());
        Assert.assertEquals("Unable to communicate with service", argumentCaptor.getValue().getMessage());
        Assert.assertEquals(testException, argumentCaptor.getValue().getCause());
    }

}