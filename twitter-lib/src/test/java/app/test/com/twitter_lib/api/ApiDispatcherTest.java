package app.test.com.twitter_lib.api;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;

import app.test.com.twitter_lib.api.auth.AuthResponse;
import app.test.com.twitter_lib.api.tweet.TweetResponse;

@RunWith(MockitoJUnitRunner.class)
public class ApiDispatcherTest {

    private ApiDispatcher subject;
    @Mock
    private ApiListener<?> apiListener;

    @Before
    public void setUp() throws Exception {
        subject = new ApiDispatcher();
        Assert.assertEquals(2, subject.getListenerMap().keySet().size());
        for (List<ApiListener<?>> list : subject.getListenerMap().values()) {
            Assert.assertTrue(list.isEmpty());
        }
    }

    @Test
    public void addListener() throws Exception {
        subject.addListener(RequestType.AUTH, apiListener);
        subject.addListener(RequestType.TWEETS, apiListener);
        for (List<ApiListener<?>> list : subject.getListenerMap().values()) {
            Assert.assertEquals(1, list.size());
        }
    }

    @Test
    public void removeListener() throws Exception {
        subject.addListener(RequestType.AUTH, apiListener);
        subject.addListener(RequestType.TWEETS, apiListener);
        subject.removeListener(apiListener);
        for (List<ApiListener<?>> list : subject.getListenerMap().values()) {
            Assert.assertTrue(list.isEmpty());
        }
    }

    @Test
    public void dispatchOnAuthSuccess() throws Exception {
        ApiListener<AuthResponse> authResponseApiListener = Mockito.mock(ApiListener.class);
        AuthResponse authResponse = new AuthResponse();
        subject.addListener(RequestType.AUTH, authResponseApiListener);
        subject.dispatchOnAuthSuccess(authResponse);
        Mockito.verify(authResponseApiListener).onSuccess(authResponse);
    }

    @Test
    public void dispatchOnAuthError() throws Exception {
        ApiListener<AuthResponse> authResponseApiListener = Mockito.mock(ApiListener.class);
        ApiException apiException = new ApiException(400, "Test");
        subject.addListener(RequestType.AUTH, authResponseApiListener);
        subject.dispatchOnAuthError(apiException);
        Mockito.verify(authResponseApiListener).onError(apiException);
    }

    @Test
    public void dispatchOnTweetSuccess() throws Exception {
        ApiListener<TweetResponse> tweetResponseApiListener = Mockito.mock(ApiListener.class);
        TweetResponse tweetResponse = new TweetResponse();
        subject.addListener(RequestType.TWEETS, tweetResponseApiListener);
        subject.dispatchOnTweetSuccess(tweetResponse);
        Mockito.verify(tweetResponseApiListener).onSuccess(tweetResponse);
    }

    @Test
    public void dispacthOnTweetError() throws Exception {
        ApiListener<TweetResponse> tweetResponseApiListener = Mockito.mock(ApiListener.class);
        ApiException apiException = new ApiException(400, "Test");
        subject.addListener(RequestType.TWEETS, tweetResponseApiListener);
        subject.dispacthOnTweetError(apiException);
        Mockito.verify(tweetResponseApiListener).onError(apiException);
    }

}