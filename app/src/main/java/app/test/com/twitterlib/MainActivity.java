package app.test.com.twitterlib;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import app.test.com.twitter_lib.TwitterClient;
import app.test.com.twitter_lib.api.ApiException;
import app.test.com.twitter_lib.api.ApiListener;
import app.test.com.twitter_lib.api.tweet.TweetResponse;
import app.test.com.twitter_lib.config.AuthConfig;
import app.test.com.twitter_lib.config.ClientConfig;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        TwitterClient twitterClient = new TwitterClient(new TwitterClient.Builder().withContext(getApplicationContext()).withClientConfig(new ClientConfigImpl()));
        twitterClient.searchTweets("#trump", 0, new ApiListener<TweetResponse>() {
            @Override
            public void onSuccess(TweetResponse data) {

            }

            @Override
            public void onError(ApiException apiException) {

            }
        });
    }


    private class ClientConfigImpl implements ClientConfig {

        @Override
        public AuthConfig getAuthConfig() {
            return new AuthConfigImpl();
        }
    }

    private class AuthConfigImpl implements AuthConfig {

        @Override
        public String getClientKey() {
            return "UMyG1eIYHYbqLvjVnp1ZDJDDR";
        }

        @Override
        public String getClientSecret() {
            return "YKnwjboBiUEOso0aASnzNWmjZSixCdIvG7yxLnfQS4lRQi7KgW";
        }
    }

}
