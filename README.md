# README #

The project is formed of the Twitter Api library and a sample app.
*  twitter-lib contains the logic associated with the Twitter REST API for authorising an app and searching for a set of tweets when given a keyword. 
*  app contains a small sample of how the Twitter library should be used.

The TwitterClient is the entry point for every API call. When creating a new TwitterClient the proper configuration must be given through a builder. The only mandatory field is the ClientConfig which clients should implement in order to supply the SDK the given info. Another field that is required is either Context or if the clients choose a separate repository for the authorisation token then they should provide an implementation to AuthRepository. The rest of the Builder fields can be left to default.

The DefaultClientStrategy can also be changed so that clients can create their own flow for searching tweets if they choose. By default the library automatically logs the application in if the token is not currently saved and after it gets the token it submits the search request, so that clients don't have to carry that in their implementations.